import axios from 'axios'
import moment from 'moment'

const url = 'http://localhost:5000'

export function getAllPosts () {
  return axios.get(`${url}/posts`)
    .then(posts => {
      return posts.data.map(post => {
        post.date = moment(post.date)
        post.truncatedBody = post.body.slice(0, 100) + '...'
        return post
      })
    })
    .catch(err => {
      throw (err)
    })
}

export function getPost (postId) {
  return axios.get(`${url}/posts/${postId}`)
    .then(post => {
      const comments = post.data.comments.map(comment => {
        comment.date = moment(comment.date)
        return comment
      }).sort(sortByDate)
      post.data.comments = comments
      return post.data
    })
    .catch(err => {
      throw (err)
    })
}

export function postComment (comment) {
  return axios.post(`${url}/comments`, comment)
    .then(newComment => {
      return newComment.data
    })
    .catch(err => {
      throw (err)
    })
}

function sortByDate (a, b) {
  return b.date.valueOf() - a.date.valueOf()
}

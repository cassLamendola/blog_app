import Vue from 'vue'

export default function ViewStatus () {
  const state = {}
  Vue.set(state, 'isShowing', false)

  state.show = () => { Vue.set(state, 'isShowing', true) }
  state.hide = () => { Vue.set(state, 'isShowing', false) }

  return state
}

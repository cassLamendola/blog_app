import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import store from './store'
import '../assets/app.scss'
import PreviewListContainer from './components/PreviewListContainer.vue'
import PostContainer from './components/PostContainer.vue'
import NotFound from './components/NotFound.vue'

Vue.use(VueRouter)

/* eslint-disable-next-line no-new */
const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', name: 'home', component: PreviewListContainer },
    { path: '/post/:id', name: 'post', component: PostContainer },
    { path: '*', component: NotFound }
  ]
})

/* eslint-disable-next-line no-new */
new Vue({
  router,
  el: '#app',
  store,
  render: h => h(App)
})

export { router }

import moment from 'moment'
import * as api from '../api'

export async function getAllPosts ({ commit }) {
  commit('setLoading')
  try {
    const posts = await api.getAllPosts()
    commit('receiveAll', posts)
  } catch (err) {
    console.log(err)
    const message = err.message.includes('404')
      ? 'We couldn\'t find any posts'
      : 'We\'re terribly sorry; something went wrong. Try refreshing the page'
    commit('setErrorMessage', message)
  }
}

export async function selectPost ({ commit }, postId) {
  commit('setLoading')
  try {
    const post = await api.getPost(postId)
    commit('selectPost', post)
    commit('receiveComments', post.comments)
  } catch (err) {
    const message = err.message.includes('404')
      ? 'Oops, it looks like that post doesn\'t exist'
      : 'We\'re terribly sorry; something went wrong. Try refreshing the page'
    commit('setErrorMessage', message)
  }
}

export async function submitComment ({ commit, state }) {
  // hard coded for now, since user accounts are not a feature
  const userIds = ['1', '2', '3', '4', '5', '6']
  const userId = userIds[Math.floor(Math.random() * userIds.length)]
  const body = state.commentDraft
  const postId = state.currentPost.id
  const date = moment()
  const newComment = { body, postId, userId, date }

  try {
    const savedComment = await api.postComment(newComment)
    commit('resetCommentDraft')
    commit('addComment', savedComment)
  } catch (err) {
    const r = confirm('An error occurred while submitting your comment. Click "ok" to try again or "cancel" to cancel')
    if (r) {
      submitComment({ commit, state })
    }
  }
}

export function handleCommentChange ({ commit }, comment) {
  commit('handleCommentChange', comment)
}

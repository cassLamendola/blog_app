import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const state = {
  isLoading: true,
  isError: false,
  posts: [
    /*
    {
      id: string,
      title: string,
      truncatedBody: string,
      body: string,
      authorId: string,
      author: {
        id: string,
        name: string,
        email: string,
        username: string
      }
    }
    */
  ],
  currentPost: {
    /*
    id: string,
    title: string,
    truncatedBody: string,
    body: string,
    authorId: string,
    author: {
      id: string,
      name: string,
      email: string,
      username: string
    }
    */
  },
  comments: [
    /*
    {
      id: string,
      userId: string,
      postId: string,
      body: string,
      date: moment.Moment,
      user: {
        id: string,
        name: string,
        email: string,
        username: string
      }
    }
    */
  ],
  commentDraft: ''
}

export default new Vuex.Store({
  state,
  actions,
  mutations
})

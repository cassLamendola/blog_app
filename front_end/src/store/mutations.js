export default {
  setLoading (state) {
    state.isLoading = true
    state.isError = false
  },

  receiveAll (state, posts) {
    state.posts = posts
    state.isLoading = false
  },

  selectPost (state, post) {
    state.currentPost = post
  },

  receiveComments (state, comments) {
    state.comments = comments
    state.isLoading = false
  },

  handleCommentChange (state, comment) {
    state.commentDraft = comment
  },

  resetCommentDraft (state) {
    state.commentDraft = ''
  },

  addComment (state, comment) {
    state.comments.unshift(comment)
  },

  setErrorMessage (state, message) {
    state.isError = true
    state.errorMessage = message
    state.isLoading = false
  }
}

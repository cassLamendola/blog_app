const moment = require('moment')
const mockData = require('./seed')

function migrateData(postsdb, usersdb, commentsdb) {
  mockData.posts.forEach(async post => {
    post.date = moment()
    postsdb.put(post.id, post, (err) => { })
  })

  mockData.users.forEach(async user => {
    usersdb.put(user.id, user, (err) => { })
  })

  mockData.comments.forEach(async comment => {
    comment.date = moment()
    commentsdb.put(comment.id, comment, (err) => { })
  })
}

module.exports = migrateData

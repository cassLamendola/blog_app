const levelup = require('levelup')
const leveldown = require('leveldown')
const sublevel = require('level-sublevel')
const migrateData = require('./migrations')

const db = sublevel(levelup(leveldown('./db'), {
  valueEncoding: 'json'
}))

const postsdb = db.sublevel('posts')
const usersdb = db.sublevel('users')
const commentsdb = db.sublevel('comments')

migrateData(postsdb, usersdb, commentsdb)

module.exports = { postsdb: postsdb, usersdb: usersdb, commentsdb: commentsdb }



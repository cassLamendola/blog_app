const commentRouter = require('express').Router()
const rand = require('random-key')
const db = require('../db')
const { commentsdb, usersdb } = db

commentRouter.route('/')
  .post(async (req, res) => {
    const newComment = { postId, body, userId, date } = req.body
    newComment.id = rand.generate()
    commentsdb.put(newComment.id, newComment, (err) => {
      if (err) {
        res.status(err.status).json(err.message)
      } else {
        // get user info
        usersdb.get(newComment.userId, (err, user) => {
          if (err) {
            res.status(err.status).json(err.message)
          } else {
            newComment.user = user
            res.status(201).json(newComment)
          }
        })
      }
    })
  })

commentRouter.route('/:id')
  .get(async (req, res) => {
    const { id } = req.params
    commentsdb.get(id, (err, comment) => {
      if (err) {
        res.status(err.status).json(err.message)
      } else {
        // get user info
        usersdb.get(comment.userId, (err, user) => {
          if (err) {
            res.status(err.status).json(err.message)
          } else {
            comment.user = user
            res.status(201).json(comment)
          }
        })
      }
    })
  })

  .put(async (req, res) => {
    const { id } = req.params
    const { editedComment } = { id, postId, body, userId, date } = req.body
    commentsdb.put(id, editedComment, err => {
      if (err) {
        res.status(err.status).json(err.message)
      } else {
        res.status(200).json(editedComment)
      }
    })
  })

  .delete(async (req, res) => {
    const { id } = req.params
    commentsdb.del(id, err => {
      if (err) {
        res.status(err.status).json(err.message)
      } else {
        res.status(200).json()
      }
    })
  })

  module.exports = commentRouter

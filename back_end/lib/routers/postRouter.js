const postRouter = require('express').Router()
const rand = require('random-key')
const db = require('../db')
const { postsdb, usersdb, commentsdb } = db

postRouter.route('/')
  .get(async (req, res) => {
    try {
      const posts = []
      const stream = await postsdb.createReadStream()
      stream.on('data', async (post) => {
        usersdb.get(post.value.authorId, (err, user) => {
          if (err) {
            res.status(err.status).json(err.message)
          } else {
            post.value.author = user
            posts.push(post.value)
          }
        })
      })
      stream.on('close', () => {
        res.status(200).json(posts)
      })
    } catch (err) {
      res.status(err.status).json(err.message)
    }
  })

  .post((req, res) => {
    const newPost = { title, body, authorId, date } = req.body
    newPost.id = rand.generate()
    postsdb.put(newPost.id, newPost, err => {
      if (err) {
        res.status(err.status).json(err.message)
      } else {
        // get author info
        usersdb.get(newPost.authorId, (err, user) => {
          if (err) {
            res.status(err.status).json(err.message)
          } else {
            newPost.author = user
            res.status(201).json(newPost)
          }
        })
      }
    })
  })

postRouter.route('/:id')
  .get((req, res) => {
    const { id } = req.params
    postsdb.get(`${id}`, async (err, post) => {
      if (err) {
        res.status(err.status).json(err.message)
      } else {
        // get author info
        usersdb.get(post.authorId, (err, user) => {
          if (err) {
            res.status(err.status).json(err.message)
          } else {
            post.author = user
          }
        })

        // get comments
        const comments = []
        const stream = await commentsdb.createReadStream()
        stream.on('data', async (comment) => {
          if (comment.value.postId === id) {
            usersdb.get(comment.value.userId, (err, user) => {
              if (err) {
                res.status(err.status).json(err.message)
              } else {
                comment.value.user = user
                comments.push(comment.value)
              }
            })
          }
        })
        stream.on('close', () => {
          post.comments = comments
          res.status(200).json(post)
        })
      }
    })
  })

  .put((req, res) => {
    const { id } = req.params
    const { editedPost } = { id, title, body, authorId, date } = req.body
    postsdb.put(id, editedPost, err => {
      if (err) {
        res.status(err.status).json(err.message)
      } else {
        res.status(200).json(editedPost)
      }
    })
  })

  .delete((req, res) => {
    const { id } = req.params
    postsdb.del(id, async (err) => {
      if (err) {
        res.status(err.status).json(err.message)
      } else {
        const posts = []
        const stream = await postsdb.createReadStream()
        // TODO: delete all comments associated with the post?
        stream.on('data', (post) => {
          usersdb.get(post.value.authorId, (err, user) => {
            if (err) {
              res.status(err.status).json(err.message)
            } else {
              post.value.author = user
              posts.push(post.value)
            }
          })
        })
        stream.on('close', () => {
          res.status(200).json(posts)
        })
      }
    })
  })

module.exports = postRouter

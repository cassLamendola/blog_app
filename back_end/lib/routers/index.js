const postRouter = require('./postRouter')
const commentRouter = require('./commentRouter')

module.exports = function (server) {
  server.get('/', (req, res) => {
    res.send({ api: 'running' })
  })

  server.use('/posts', postRouter)
  server.use('/comments', commentRouter)
}


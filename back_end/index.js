const express = require('express')
const cors = require('cors')
const setUpRoutes = require('./lib/routers')

const server = express()
server.use(express.json())
server.use(cors({ origin: 'http://localhost:8080' }))

setUpRoutes(server)

server.listen(5000, () => {
  console.log('*** Listening on port 5000 ***')
})
# blog_app

## Getting Started
### Start up backend server
- Switch to directory: `back-end`
- Install dependencies: `yarn`
- Star the server: `yarn start`

### Start up frontend project
- In a new tab, switch to directory: `front-end`
- Install dependencies: `yarn`
- Start the project for development with: `yarn start`

## Linting
- Run ESLint with: `yarn lint` to apply standard linting rules and recommended vue.js rules
- Fix linting errors with `yarn lint:fix`